<?php
/**
 * Enqueue scripts and styles.
 */
function alyanna_scripts() {
  wp_enqueue_style( 'alyanna-style', get_stylesheet_uri() );
  wp_enqueue_style( 'alyanna-style-min', get_template_directory_uri() . '/css/style.min.css', array(), '1.0.7' );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'alyanna_scripts' );
