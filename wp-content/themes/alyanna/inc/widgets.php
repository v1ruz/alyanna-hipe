<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function alyanna_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Sidebar', 'alyanna' ),
    'id'            => 'sidebar-1',
    'description'   => esc_html__( 'Add widgets here.', 'alyanna' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Header - Social Media', 'alyanna' ),
    'id'            => 'social-media',
    'description'   => esc_html__( 'Add widgets here.', 'alyanna' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="social-media">',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Footer - Social Media', 'alyanna' ),
    'id'            => 'footer-social-media',
    'description'   => esc_html__( 'Add widgets here.', 'alyanna' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="footer-social-media">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Footer - Send Note', 'alyanna' ),
    'id'            => 'footer-sendnote',
    'description'   => esc_html__( 'Add widgets here.', 'alyanna' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="footer-sendnote">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Footer - Popular Posts', 'alyanna' ),
    'id'            => 'footer-popular',
    'description'   => esc_html__( 'Add widgets here.', 'alyanna' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="footer-popular">',
    'after_title'   => '</h3>',
  ) );
}
add_action( 'widgets_init', 'alyanna_widgets_init' );
