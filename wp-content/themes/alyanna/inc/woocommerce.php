<?php
// remove hooks
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_action('woocommerce_shop_loop_item_title', 'custom_loop_item_title');
function custom_loop_item_title()
{
  echo '<h3 class="woocommerce-loop-product__title">' . get_the_title() . '</h3>';
}

add_action('woocommerce_before_main_content', 'product_wrapper_start');
function product_wrapper_start()
{
  echo '<hr><div class="container">';
}

add_action('woocommerce_after_main_content', 'product_wrapper_end');
function product_wrapper_end()
{
  echo '</div>';
}

add_filter('single_product_archive_thumbnail_size', 'always_full');
function always_full()
{
  return 'full';
}
