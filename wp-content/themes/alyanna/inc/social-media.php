<?php

add_shortcode('social-media', function() {
  ob_start();
  get_template_part('template-parts/social', 'media');
  return ob_get_clean();
});
