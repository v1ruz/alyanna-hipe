<?php

function triggerPopularPost($post_id) {
  $count_key = 'popular_posts';
  $count = get_post_meta($post_id, $count_key, true);
  if ($count == '') {
    $count = 0;
    delete_post_meta($post_id, $count_key);
    add_post_meta($post_id, $count_key, '0');
  } else {
    $count++;
    update_post_meta($post_id, $count_key, $count);
  }
}

add_action('wp_head', 'trackPosts');
function trackPosts($post_id) {
  if (!is_single()) return;
  if (is_user_logged_in()) return;

  if (empty($post_id)) {
    global $post;
    $post_id = $post->ID;
  }

  triggerPopularPost($post_id);
}

add_shortcode('popular-posts', function() {
  ob_start();
  get_template_part('template-parts/popular-posts');
  return ob_get_clean();
});
