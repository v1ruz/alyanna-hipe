<?php get_header() ?>
<div class="container">
<div id="content" class="page">
<?php if (have_posts()): ?>
  <?php while (have_posts()): the_post(); ?>
    <h1><?php the_title() ?></h1>
    <hr>
    <?php the_content(); ?>
  <?php endwhile; ?>
<?php endif; ?>
</div>
</div>
<?php get_footer() ?>
