<?php

include 'inc/initial-setup.php';
include 'inc/widgets.php';
include 'inc/scripts.php';
include 'inc/social-media.php';
include 'inc/popular-posts.php';
include 'inc/woocommerce.php';

add_filter('comment_form_default_fields', 'remove_comment_fields');
function remove_comment_fields($fields) {
  unset($fields['url']);
  return $fields;
}

function archive_pagination() {
  global $wp_query;

  $big = 999999999; // need an unlikely integer

  echo '<div class="pages">';
  echo paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $wp_query->max_num_pages,
    'type' => 'list'
  ) );
  echo '</div>';
}
