<?php get_header(); ?>
<div id="content">
  <div class="container">
    <h1><?php the_archive_title() ?></h1>
    <hr>
    <div class="row">
      <div class="col-md-8">
        <?php #$paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
        <?php #query_posts(array('posts_per_page' => 2, 'paged' => $paged)) ?>
        <?php if (have_posts()): ?>
          <div class="row">
          <?php
            while (have_posts()) { the_post();
              get_template_part('template-parts/article', 'halfwidth');

              if ($count != 1 && $count % 2 === 1) { echo '</div><div class="row">'; }
              $count++;
            }
          ?>
          </div>
          <?php archive_pagination() ?>
        <?php endif; ?>
      </div>
      <div class="col-md-4">
        <?php get_sidebar() ?>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>
