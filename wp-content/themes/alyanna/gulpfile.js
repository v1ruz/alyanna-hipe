var gulp = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('css', function() {
  return gulp.src('sass/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
        browsers: ['last 20 versions', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4']
    }))
    .pipe(cleancss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('css'))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('css'));
});

gulp.task('icons', function() {
  return gulp.src('bower_components/bootstrap-sass/assets/fonts/bootstrap/**.*')
    .pipe(gulp.dest('fonts'));
});

gulp.task('font-awesome', function() {
  return gulp.src('bower_components/components-font-awesome/fonts/**.*')
    .pipe(gulp.dest('fonts'));
});

gulp.task('watch', function() {
  gulp.watch('sass/**/*.scss', function() {
    gulp.run('css');
  });
});

gulp.task('default', function() {
  gulp.run('icons');
  gulp.run('font-awesome');
  gulp.run('css');
  gulp.run('watch');
});
