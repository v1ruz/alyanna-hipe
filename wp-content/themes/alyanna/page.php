<?php
if (function_exists('is_woocommerce')) {
  if (is_woocommerce() || is_page('cart') || is_page('checkout') || is_page('my-account')) {
    get_template_part('index');
  } else {
    get_template_part('single');
  }
} else {
  get_template_part('single');
}
