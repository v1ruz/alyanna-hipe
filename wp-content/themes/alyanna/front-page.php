<?php
get_header();

$count = 1;
$posts = get_posts(array('posts_per_page' => 5));
?>
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <?php if (count($posts)): ?>
            <div class="row">
            <?php
              foreach ($posts as $post) { setup_postdata($post);
                if ($count === 1) {
                  get_template_part('template-parts/article', 'fullwidth');
                } else {
                  get_template_part('template-parts/article', 'halfwidth');
                }
                if ($count != 1 && $count % 2 === 1) { echo '</div><div class="row">'; }
                $count++;
              }
            ?>
            </div>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>
        </div>
        <div class="col-md-4">
          <?php get_sidebar() ?>
        </div>
      </div>
    </div>
  </div>
  <section id="about">
    <?php $about = get_post(2); ?>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="profile-container">
            <a href="<?php echo get_the_permalink($about) ?>">
              <?php echo get_the_post_thumbnail($about) ?>
            </a>
          </div>
          <div class="profile-content">
            <h2><?php echo $about->post_title ?></h2>
            <p>
              <?php echo wp_trim_words(strip_shortcodes($about->post_content), 150); ?>
            </p>
            <p><a href="<?php echo get_the_permalink($about) ?>" class="btn btn-primary">Read More</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
  $meta_query  = WC()->query->get_meta_query();
  $tax_query   = WC()->query->get_tax_query();
  $tax_query[] = array(
    'taxonomy' => 'product_visibility',
    'field'    => 'name',
    'terms'    => 'featured',
    'operator' => 'IN',
  );
  $featured = get_posts(array(
    'post_type'   =>  'product',
    'posts_per_page'   =>  1,
    'meta_query'  =>  $meta_query,
    'tax_query'  =>  $tax_query
  ));
  ?>
  <section id="featured-book">
    <div class="container">
      <h2>Featured Book</h2>
      <?php if (count($featured)) : ?>
        <?php foreach ($featured as $post): setup_postdata($post); ?>
        <div class="book-information">
          <div class="row">
            <div class="col-md-4">
              <div class="img">
                <?php $img = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full', true); ?>
                <img src="<?php echo $img[0] ?>" alt="<?php the_title() ?>">
              </div>
            </div>
            <div class="col-md-8">
              <h3 class="h1"><?php the_title() ?></h3>
              <?php the_excerpt() ?>
              <a href="<?php the_permalink() ?>" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>
        <?php endforeach; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
  </section>
<?php get_footer() ?>
