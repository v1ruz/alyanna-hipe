  <footer id="footer">
    <div class="footer-container">
      <div class="container">
        <div class="row">
          <div class="col-sm-4 section-address">
            <?php
            get_template_part('template-parts/sitename');

            if (is_active_sidebar('footer-social-media')) {
              dynamic_sidebar('footer-social-media');
            }

            get_template_part('template-parts/my-account'); ?>
          </div>

          <div class="col-sm-4 section-contact">
            <?php
            if (is_active_sidebar('footer-sendnote')) {
              dynamic_sidebar('footer-sendnote');
            } ?>
          </div>
          <div class="col-sm-4 section-popular">
            <?php
            if (is_active_sidebar('footer-popular')) {
              dynamic_sidebar('footer-popular');
            } ?>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <?php wp_footer(); ?>
</body>
</html>
