<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <header class="header-navigation">
    <div class="container">
      <div class="row">
        <nav id="site-navigation" class="col-xs-12 main-navigation">
        <?php
          wp_nav_menu( array(
            'theme_location' => 'header',
            'menu_id'        => 'primary-menu',
          ) );
        ?>
          <div class="pull-right">
            <?php get_template_part('template-parts/social', 'media') ?>
          </div>
        </nav>
      </div>
    </div>
  </header>
  <div class="container">
    <div class="row">
      <div class="site-logo">
        <?php get_template_part('template-parts/sitename') ?>
      </div>
    </div>
  </div>
  <?php if ((is_front_page() || is_page()) && !is_page('about')) : ?>
  <?php if ($thumb_id = get_post_thumbnail_id()) : ?>
  <?php $img = wp_get_attachment_image_src($thumb_id, 'full', true); ?>
  <div class="container-fluid">
    <div class="row">
      <div class="banner" style="background-image: url(<?php echo $img[0] ?>)"></div>
    </div>
  </div>
  <?php endif; ?>
  <?php endif; ?>
