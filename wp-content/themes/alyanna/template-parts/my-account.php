<div class="footer-myaccount">
  <?php $myaccount = wc_get_page_permalink( 'myaccount' ); ?>
  <a href="<?php echo $myaccount ?>">My Account</a>
  <?php if (is_user_logged_in()): ?>
  | <a href="<?php echo wc_logout_url($myaccount) ?>">Logout</a>
  <?php endif; ?>
</div>
