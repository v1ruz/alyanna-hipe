<div class="col-md-12">
  <article class="featured-post">
    <div class="featured-image">
      <a href="<?php the_permalink() ?>"><?php the_post_thumbnail() ?></a>
    </div>
    <div class="content">
      <h1><?php the_title() ?></h1>
      <span class="meta">
        <?php the_author() ?> &#8226; <?php echo get_the_date('F d Y') ?> &#8226; <?php comments_number(); ?>
      </span>
      <?php the_excerpt() ?>
      <a href="<?php the_permalink() ?>" class="btn btn-primary">Read more</a>
    </div>
  </article>
</div>
