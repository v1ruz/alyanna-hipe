<?php
$facebook = get_field('facebook', 'options');
$twitter = get_field('twitter', 'options');
$instagram = get_field('instagram', 'options');
$youtube = get_field('youtube', 'options');
?>
<div class="social-media">
  <ul>
    <li><a href="<?php echo $facebook ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    <li><a href="<?php echo $twitter ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
    <li><a href="<?php echo $instagram ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
    <li><a href="<?php echo $youtube ?>" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
  </ul>
</div>
