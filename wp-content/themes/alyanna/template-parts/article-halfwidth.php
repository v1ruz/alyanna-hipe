<div class="col-md-6">
  <article class="featured-post">
    <div class="featured-image">
      <a href="<?php the_permalink() ?>" class="pagelink"><?php the_post_thumbnail() ?></a>
    </div>
    <div class="content">
      <a href="<?php the_permalink() ?>" class="pagelink"><h3><?php the_title() ?></h3></a>
      <span class="meta">
        <?php the_author() ?> &#8226; <?php echo get_the_date('F d Y') ?> &#8226; <?php comments_number(); ?>
      </span>
      <?php the_excerpt() ?>
    </div>
  </article>
</div>
