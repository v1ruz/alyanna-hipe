<div id="content">
  <hr>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="featured-image">
          <?php the_post_thumbnail('large') ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8">
        <?php
        if (have_posts()) {
          while (have_posts()) { the_post();
            get_template_part('template-parts/article');
            the_post_navigation();
            comments_template();
          }
        } else {
          get_template_part('template-parts/article', 'zero-result');
        }
        ?>
      </div>
      <div class="col-md-4">
        <?php get_sidebar() ?>
      </div>
    </div>
  </div>
</div>
