<?php
$popular = get_posts(array(
  'posts_per_page' => 3,
  'post_type' => 'post',
  'meta_key' => 'popular_posts',
  'orderby' => 'meta_value_num',
  'order' => 'DESC'
));

if (count($popular)) {
  foreach ($popular as $p) { ?>
    <div class="row">
      <div class="col-xs-4">
        <div class="img">
          <a href="<?php echo get_the_permalink($p) ?>">
            <?php echo get_the_post_thumbnail($p, 'thumbnail') ?>
          </a>
        </div>
      </div>
      <div class="col-xs-8">
        <a href="<?php echo get_the_permalink($p) ?>">
          <span class="title"><?php echo get_the_title($p) ?></span>
        </a>
        <strong class="date"><?php echo get_the_date('', $p->ID) ?></strong>
      </div>
    </div>
    <?php
  }
}
