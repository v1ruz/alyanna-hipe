<article class="post-<?php the_ID() ?>">
  <div class="content">
    <h1 class="page-title"><?php the_title() ?></h1>
    <?php get_template_part('template-parts/meta'); ?>
    <?php the_content() ?>
  </div>
</article>
<section class="about-author">
  <div class="row">
    <div class="col-md-2">
      <?php echo get_wp_user_avatar(get_the_author_meta('ID'), 'full') ?>
    </div>
    <div class="col-md-10">
      <span class="author">Author: <?php the_author() ?></span>
      <?php echo wpautop(get_the_author_meta('description')) ?>
    </div>
  </div>
</section>
<hr>
