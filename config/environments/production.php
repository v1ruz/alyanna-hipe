<?php

use Roots\WPConfig\Config;

Config::define('WP_DEBUG', true);
Config::define('SCRIPT_DEBUG', true);
Config::define('DISALLOW_FILE_MODS', false);

ini_set('error_log', basename(Config::get('CONTENT_DIR')) . '/error.log');
